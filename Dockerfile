FROM golang:1.18 AS compile
ADD . /root
WORKDIR /root
ENV CGO_ENABLED=0
RUN go build -o homewizard-exporter

FROM alpine:latest
COPY --from=compile /root/homewizard-exporter /usr/local/bin/
ENTRYPOINT /usr/local/bin/homewizard-exporter