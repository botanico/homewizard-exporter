package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Client is responsible to interact with Homewizard P1 energy monitor
type Client struct {
	Host   string
	Client *http.Client
}

// Data data
type Data struct {
	WifiSSID              string  `json:"wifi_ssid"`
	WifiStrength          float64 `json:"wifi_strength"`
	TotalPowerImportT1Kwh float64 `json:"total_power_import_t1_kwh"`
	TotalPowerExportT1Kwh float64 `json:"total_power_export_t1_kwh"`
	ActivePowerW          float64 `json:"active_power_w"`
	ActivePowerL1W        float64 `json:"active_power_l1_w"`
}

// NewHomeWizardClient creates a new client with a specified http client
func NewHomeWizardClient(host string, httpClient *http.Client) Client {
	return Client{
		Host:   host,
		Client: httpClient,
	}
}

// Retrieve data from P1
func (c *Client) Retrieve() (*Data, error) {
	url := fmt.Sprintf("http://%s/api/v1/data", c.Host)

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	res, err := c.Client.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	var data Data

	err = json.NewDecoder(res.Body).Decode(&data)
	if err != nil {
		return nil, err
	}

	return &data, nil
}
