package main

import (
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Gauges struct {
	WifiStrength          *prometheus.GaugeVec
	TotalPowerImportT1Kwh *prometheus.GaugeVec
	TotalPowerExportT1Kwh *prometheus.GaugeVec
	ActivePowerW          *prometheus.GaugeVec
}

func main() {
	gauges := Gauges{
		WifiStrength: prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Name: "wifi_strength",
			Help: "Wifi strength in Db",
		}, []string{"ip"}),
		TotalPowerImportT1Kwh: prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Name: "total_power_import_t1_kwh",
			Help: "The total power import on T1 in kWh",
		}, []string{"ip"}),
		TotalPowerExportT1Kwh: prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Name: "total_power_export_t1_kwh",
			Help: "The total power export on T1 in kWh",
		}, []string{"ip"}),
		ActivePowerW: prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Name: "active_power_w",
			Help: "The active power in W",
		}, []string{"ip"}),
	}

	prometheus.MustRegister(gauges.WifiStrength)
	prometheus.MustRegister(gauges.TotalPowerExportT1Kwh)
	prometheus.MustRegister(gauges.TotalPowerImportT1Kwh)
	prometheus.MustRegister(gauges.ActivePowerW)

	http.Handle("/metrics", promhttp.Handler())

	go gauges.Loop()

	log.Fatal(http.ListenAndServe(":9000", nil))
}

func (g *Gauges) Loop() {
	timer := time.NewTicker(15 * time.Second)

	clients := map[string]Client{}

	httpClient := &http.Client{
		Timeout: time.Second * 5,
	}

	for _, ip := range strings.Split(os.Getenv("HOST"), " ") {
		clients[ip] = NewHomeWizardClient(ip, httpClient)
	}

	for range timer.C {
		for ip, client := range clients {
			data, err := client.Retrieve()
			if err != nil {
				log.Printf("ERR: %s", err)

				continue
			}

			g.Set(ip, data)
		}
	}
}

func (g *Gauges) Set(ip string, data *Data) {
	labels := prometheus.Labels{
		"ip": ip,
	}

	g.WifiStrength.With(labels).Set(data.WifiStrength)
	g.TotalPowerExportT1Kwh.With(labels).Set(data.TotalPowerExportT1Kwh)
	g.TotalPowerImportT1Kwh.With(labels).Set(data.TotalPowerImportT1Kwh)
	g.ActivePowerW.With(labels).Set(data.ActivePowerW)
}
